Name:           OpenEXR
Summary:        A high dynamic-range (HDR) image file format for use in computer imaging applications
Version:        3.1.5
Release:        2
License:        BSD-3-Clause
URL:	        http://www.openexr.com/
Source0:        https://github.com/AcademySoftwareFoundation/openexr/archive/v%{version}/openexr-%{version}.tar.gz
BuildRequires:  gcc-c++ zlib-devel pkgconfig python3-devel
BuildRequires:  cmake gcc boost-devel pkgconfig(Imath) 

Requires:       %{name}-libs = %{version}-%{release}

Provides:       openexr = %{version}-%{release}
Obsoletes:      openexr < %{version}-%{release}

%description
OpenEXR is a high dynamic-range (HDR) image file format originally developed by Industrial
Light & Magic for use in computer imaging applications.

%package        libs
Summary:        Libraries for %{name}
Provides:       openexr-libs = %{version}-%{release}
Obsoletes:      openexr-libs < %{version}-%{release}

%description    libs
Libraries for %{name}.

%package devel
Summary:        Development files for %{name}
Provides:       openexr-devel = %{version}-%{release}
Obsoletes:      openexr-devel < %{version}-%{release}
Provides:       ilmbase-devel = %{version}-%{release}
Obsoletes:      ilmbase-devel < 2.5.3
Requires:       %{name}-libs = %{version}-%{release}

%description devel
This package contains libraries and header files for development of %{name}.

%prep
%autosetup -n openexr-%{version} -p1

%build
%cmake
%make_build

%install
%make_install

%check
%make_build test

%ldconfig_scriptlets libs

%files
%doc CHANGES.md CONTRIBUTING.md GOVERNANCE.md SECURITY.md CODE_OF_CONDUCT.md CONTRIBUTORS.md README.md
%license LICENSE.md
%{_bindir}/*
%exclude %{_docdir}/%{name}-%{version}

%files libs
%{_libdir}/*.so.30*

%files devel
%{_docdir}/OpenEXR/
%{_includedir}/OpenEXR/
%{_libdir}/*.so
%{_libdir}/cmake/OpenEXR/
%{_libdir}/pkgconfig/OpenEXR.pc

%changelog
* Tue May 10 2022 Ge Wang <wangge20@h-partners.com> - 3.1.5-2
- license compliance rectification

* Thu Apr 28 2022 wangkai <wangkai385@h-partners.com> - 3.1.5-1
- update to 3.1.5 for fix CVE-2022-45942

* Tue Mar 29 2022 liyanan <liyanan32@huawei.com> - 3.1.3-1
- update to 3.1.3

* Wed Mar 23 2022 yaoxin <yaoxin30@huawei.com> - 2.2.0-25
- Fix CVE-2021-20299

* Fri Mar 11 2022 yaoxin <yaoxin30@huawei.com> - 2.2.0-24
- Fix CVE-2021-20303

* Wed Sat 1 2021 liwu<liwu13@huawei.com> - 2.2.0-23
- fix CVE-2021-3605

* Mon Jul 12 2021 yaoxin <yaoxin30@huawei.com> - 2.2.0-22
- fix CVE-2020-11758 CVE-2020-11759 CVE-2020-11760 CVE-2020-11761 CVE-2020-11762 CVE-2020-11763 CVE-2020-11764 CVE-2020-11765 CVE-2020-15305 CVE-2020-15306

* Sat Jul 10 2021 wangyue <wangyue92@huawei.com> - 2.2.0-21
- fix CVE-2021-3598

* Tue Jun 22 2021 houyingchao <houyingchao@huawei.com> - 2.2.0-20
- fix CVE-2021-23215 CVE-2021-23169 CVE-2021-26260

* Tue Apr 06 2021 wangyue <wangyue92@huawei.com> - 2.2.0-19
- fix CVE-2021-3474 CVE-2021-3477 CVE-2021-3476 CVE-2021-3475 CVE-2021-20296 CVE-2021-3479 CVE-2021-20296

* Wed Jan 27 2021 zhanghua <zhanghua40@huawei.com> - 2.2.0-18
- Type:CVE
- ID:NA
- SUG:NA
- DESC:fix CVE-2017-9110 CVE-2017-9111 CVE-2017-9112 CVE-2017-9113
  CVE-2017-9114 CVE-2017-9115 CVE-2017-9116 CVE-2017-12596

* Tue Feb 18 2020 hexiujun <hexiujun1@huawei.com> - 2.2.0-17
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:unpack libs subpackage

* Fri Oct 25 2019 huzhiyu <huzhiyu1@huawei.com> - 2.2.0-16
- Package init
